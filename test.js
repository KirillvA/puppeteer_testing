const puppeteer = require("puppeteer");
const config = require('./config');
const loginData = config;

var userID = process.argv[2];
var userName = process.argv[3];
var theme = process.argv[4];
var doc = process.argv[5];

let scrape = async () => {

  const browser = await puppeteer.launch({ headless: false });

  const page = await browser.newPage();
  var sendMessage;
  var main;
  var addressbook;
  browser.on("targetcreated", target => {
    target.page().then(
      result => {
        var popupPage = result;
        popupPage.title().then(
          titleString => {
            switch (titleString) {
              case "Сетевой Город. Образование. Просмотр почтовых сообщений":
                console.log("view");
                popupPage.on("load", () => {
                  main = popupPage;
                  popupPage.evaluate(() => {
                    composemessage();
                    // })
                  });
                });
                break;
              case "Сетевой Город. Образование. Отправить сообщение":
              sendMessage = popupPage;
                popupPage.on("load", () => {
                  popupPage.evaluate(() => {
                    openAddressBook("ATO", "LTO");
                    // })
                  });
                });
                break;
              case "Адресная книга":
              addressbook = popupPage;
                popupPage.on("load", () => {
                  console.log("loaded");
                  popupPage.evaluate(
                    (userName, userID) => {

                      parent.frames["addrbkleft"].AddBk(userID, userName);
                      setTimeout(function() {
                        parent.frames["addrbkright"].SetAddress();
                      }, 2000);
                    },
                    userName,
                    userID
                  );
                });
                break;

              default:
                console.log(titleString);
            }
          },
          error => {
            console.log(error);
          }
        );
      },
      error => {
        console.log("Rejected: " + error);
      }
    );
  });

  browser.on("targetdestroyed", target => {
    target.page().then(
      res => {
        sendMessage.screenshot({ path: "sendMessage.png" });
        sendMessage.click("#attach");
        setTimeout(function() {
          var dropZoneInput = sendMessage.$("#fileupload");
          dropZoneInput.then(
            dropzone => {
              dropzone.uploadFile(doc);
              setTimeout(function() {
                sendMessage.click(".bootstrap-dialog-footer .btn-primary");
                setTimeout(function() {
                    sendMessage.click(".bootstrap-dialog-footer .btn-default");
                    setTimeout(function() {
                        sendMessage.type('[name="SU"]', theme);
                        setTimeout(function() {
                            sendMessage.click('[name="NEEDNOTIFY"]');
                            setTimeout(function() {
                                //sendMessage.click('[title="Отправить"]');
                                //setTimeout(function() {
                                //    browser.close();
                                //}, 2000);
                            }, 2000);
                        }, 2000);
                    }, 2000);
                }, 5000);
              }, 2000);
            },
            error => {
              console.log("Rejected: " + error);
            }
          );
        }, 2000);
      },
      error => {
        console.log("Rejected: " + error);
      }
    );
  });

  await page.goto("http://net.citycheb.ru/");


  await page.waitForSelector("input[name='UN']");

  await page.waitFor(500);
  await page.select("select#funcs", "1");
  await page.waitFor(1000);
  await page.select("select#schools", "140");
  await page.waitFor(500);
  await page.click("input[name='UN']");
  await page.type("input[name='UN']", loginData.name);
  await page.click("input[name='PW']");
  await page.type("input[name='PW']", loginData.password);
  await page.click(".button-login");

  await page.waitForSelector(".alert");
  await page.waitFor(1000);
  await page.evaluate(() => {
    if (typeof doContinue == "function") doContinue();
  });
  await page.waitForSelector(".pull-right");
  await page.waitFor(1000);
  await page.evaluate(() => {
    sys.mail.inbox();
  });

};

scrape().then(value => {
  console.log(value); // Получилось!
});
